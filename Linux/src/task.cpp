#include <vector>
#include <valarray>
#include <iostream>
#include <cmath>

#include <thread>
#include <chrono>
#include <signal.h>

#include <opencv2/opencv.hpp>

#include "Task.hpp"

bool run = true;
void sigintCallback(int signum)
{
    std::cout << "SIGINT" << std::endl;
    run = false;
}

void solve()
{
    // Инициализация переменных
    cv::Mat scene, mapWithZones;
    Task sim;

    // Запуск симуляции
    sim.start();

    mapWithZones = sim.getTaskMapWithZones();
    

    // Информации о роботе
    std::cout << "Robots number: " << sim.robots.size() << std::endl;
    std::cout << "Robot 0" << std::endl;
    std::cout << "X coordinate of robot with index 0 " << sim.robots[0]->state.x << std::endl;
    std::cout << "Y coordinate of robot with index 0" << sim.robots[0]->state.y << std::endl;
    std::cout << "Angle between course angle of robot and X-axis of robot with index 0" << sim.robots[0]->state.steerAngle << std::endl;
   
    // Чтобы получить координаты синих кругов выполните:
    std::vector <cv::Point> circles = sim.getBoxes();
    std::cout << circles[0].x << " " << circles[0].y << std::endl; // координаты круга с индексом 0
    std::valarray<double> voltage = {0, 0};

    cv::namedWindow("Scene", cv::WINDOW_NORMAL);
    while (run) {
        // Подача напряжения на колеса робота, если подаете два положительных значения - робот едет вперед, если два отрицатеьльных - робот поедет назад
        // Если подать два числа с разными знаками - робот начнет поворачивать.
        voltage[0] = 0;
        voltage[1] = 10;

        sim.robots[0]->setMotorVoltage(voltage);

        scene = sim.getTaskScene();
        cv::imshow("Scene", scene);
        cv::waitKey(10); // ms
    }

    sim.stop(); // Остановка симуляции (прерывание теста)
}

int main()
{
    // Чтобы закончить выполнение симулятора зажмите Ctrl + C
    signal(SIGINT, sigintCallback);

    solve();
    return 0;
}
