cmake_minimum_required(VERSION 3.8 FATAL_ERROR)
project(sicamp_sim LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -pipe -O2")
set(CMAKE_CXX_COMPILER g++)

## Find libraries and modules
find_package(OpenCV REQUIRED)
find_package(Threads)

include_directories(
    include
    ${OpenCV_INCLUDE_DIRS}
)
link_directories(lib)

## Executables
add_executable(task
    src/task.cpp
)
target_link_libraries(task
    sicamp_sim sicamp_task ${OpenCV_LIBS} Threads::Threads
)
