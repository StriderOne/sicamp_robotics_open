import sys
sys.path.insert(0, '../lib/')

from py_sicamp_task import Task
import cv2

import signal

run = False

def SIGINT_handler(signum, frame):
    global run
    print('SIGINT')
    run = False
## Здесь должно работать ваше решение
def solve():
    global run

    ## Запуск задания и таймера (внутри задания)
    task = Task()
    task.start()
    run = True

    ## Загружаем изображение из задачи
    # сцена отправляется с определенной частотй
    # для этого смотри в документацию к задаче
    mapWithZones = task.getTaskMapWithZones()
    sceneImg     = task.getTaskScene()
    # Массив роботов
    robots = task.getRobots()
    print('Number of robots:', len(robots))
    print("Robot 0")
    print("X coordinate of robot with index 0", robots[0].state.x)
    print("Y coordinate of robot with index 0", robots[0].state.y)
    print("Angle between course angle of robot and X-axis of robot with index 0", robots[0].state.steerAngle)
    # Чтобы получить координаты синих кругов выполните:
    circles = task.getBoxes()
    print(circles[0][0], circles[0][1]) # координаты круга с индексом 0
    while (run):
        # Подача напряжения на колеса робота, если подаете два положительных значения - робот едет вперед, если два отрицатеьльных - робот поедет назад
        # Если подать два числа с разными знаками - робот начнет поворачивать.
        v = [10, 0]
        robots[0].setMotorVoltage(v)
        sceneImg = task.getTaskScene()
        cv2.imshow('Scene', sceneImg)
        cv2.waitKey(20) # мс

    task.stop()

if __name__ == '__main__':
    # Чтобы закончить выполнение симулятора зажмите Ctrl + C
    signal.signal(signal.SIGINT, SIGINT_handler)
    solve()
