#pragma once

#include <opencv2/opencv.hpp>
#include "Robot.hpp"
#include <string>


class Simulation;
class GrayRobotC;
class RedRobotC;

class Checker;


class Task
{
private:

    
    GrayRobotC* grayRobot;
    RedRobotC* redRobot;
    Checker* checker;

public:
    Simulation* sim;
    Task();
    ~Task();

    void start();
    void stop();
    void complete();

    void startCheck();
    void stopCheck();
    
    std::vector<Robot*> getRobots() {return robots;}

    cv::Mat getTaskScene();
    cv::Mat getTaskMapWithZones();

    std::vector<Robot*> robots;
    std::vector <cv::Point> getBoxes();
};
